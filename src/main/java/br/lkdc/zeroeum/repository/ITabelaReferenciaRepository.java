package br.lkdc.zeroeum.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.lkdc.zeroeum.model.TabelaReferencia;

public interface ITabelaReferenciaRepository extends CrudRepository<TabelaReferencia, BigInteger> {

      @Query(value = "SELECT max(t.codigo) FROM tabela_referencia  t", nativeQuery = true)
	  public Integer codigoAtualTabelaReferenciada();
}
