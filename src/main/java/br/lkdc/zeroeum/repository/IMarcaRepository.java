package br.lkdc.zeroeum.repository;

import java.math.BigInteger;

import org.springframework.data.repository.CrudRepository;

import br.lkdc.zeroeum.model.Marca;

public interface IMarcaRepository extends CrudRepository<Marca, BigInteger> {

}
