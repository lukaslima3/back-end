package br.lkdc.zeroeum.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import br.lkdc.zeroeum.model.Marca;
import br.lkdc.zeroeum.model.TabelaReferencia;
import br.lkdc.zeroeum.model.Veiculo;
import br.lkdc.zeroeum.repository.IMarcaRepository;
import br.lkdc.zeroeum.service.MarcaService;
import br.lkdc.zeroeum.service.TabelaRefereciaService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class Teste {
	
	@Autowired
	private MarcaService marcaService;
	
	@Autowired
	private TabelaRefereciaService tabelaRefereciaService;
	
	private static final String CONSULTAR_MARCAS_FIPE = "http://veiculos.fipe.org.br/api/veiculos/ConsultarMarcas";

	private static final String CONSULTAR_TABELA_REFERENCIA_FIPE = "http://veiculos.fipe.org.br/api/veiculos/ConsultarTabelaDeReferencia";
	@RequestMapping("/home")
	public ResponseEntity<Veiculo> indexPage(@RequestBody Veiculo veiculo) {
		veiculo.setFabricante("Chery");
		return new ResponseEntity<Veiculo>(veiculo, 
			      HttpStatus.OK);
	}
	@RequestMapping("/marcas")
	public ResponseEntity<List<Marca>> listAll() {
		return new ResponseEntity<List<Marca>>(marcaService.findAll(), 
				HttpStatus.OK);
	}
	@RequestMapping("/fipe")
	public ResponseEntity<List<Marca>> indexPage() {
		String url = CONSULTAR_MARCAS_FIPE;

        HttpURLConnection conn;
		try {
			conn = montarRequest(url);
			JsonObject cred = new JsonObject();
			cred.addProperty("codigoTabelaReferencia",231);
			cred.addProperty("codigoTipoVeiculo", 1);
			PrintStream printStream = new PrintStream(conn.getOutputStream());
			printStream.println(cred);
			if (conn.getResponseCode() != 200) {
                System.out.println("Erro " + conn.getResponseCode() + " ao obter dados da URL " + url);
            }
			List<Marca> dados = extrairResponse(conn);
			marcaService.salvar(dados);
	            return new ResponseEntity<List<Marca>>(dados, 
	    				HttpStatus.OK);
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        
		return new ResponseEntity<List<Marca>>(new ArrayList<Marca>(), 
				HttpStatus.OK);
	}
	
	
	@RequestMapping("/consultarTabelaDeReferencia")
	public ResponseEntity<Object> carregarTabelaDeReferencia() {
		String url = CONSULTAR_TABELA_REFERENCIA_FIPE;
		
		HttpURLConnection conn;
		try {
			conn = montarRequest(url);
			JsonObject cred = new JsonObject();
			cred.addProperty("codigoTabelaReferencia",231);
			cred.addProperty("codigoTipoVeiculo", 1);
			PrintStream printStream = new PrintStream(conn.getOutputStream());
			if (conn.getResponseCode() != 200) {
				System.out.println("Erro " + conn.getResponseCode() + " ao obter dados da URL " + url);
			}
			List<TabelaReferencia> dados = extrairResponseList(conn);
			tabelaRefereciaService.validarSalvarNovos(dados);
			
			return new ResponseEntity<Object>(dados, 
					HttpStatus.OK);
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    
	return new ResponseEntity<Object>(new Object(), 
			HttpStatus.OK);
}

	private HttpURLConnection montarRequest(String url) throws IOException, MalformedURLException, ProtocolException {
		HttpURLConnection conn;
		System.setProperty("http.agent", "PostmanRuntime/7.28.0");
		conn = (HttpURLConnection) new URL(url).openConnection();
		conn.addRequestProperty("User-Agent", "PostmanRuntime/7.28.0");
		conn.setDoOutput(true);
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("Accept", "application/json");
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Accept", "application/json");
		return conn;
	}
	private List<Marca> extrairResponse(HttpURLConnection conn) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
		 String output = "";
		    String line;
		    while ((line = br.readLine()) != null) {
		        output += line;
		    }

		    conn.disconnect();
		    ObjectMapper objectMapper = new ObjectMapper();
		    objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		    Marca[] marcas = objectMapper.readValue(new String(output.getBytes()),  Marca[].class);
		return Arrays.asList(marcas);
	}
	
	private List<TabelaReferencia> extrairResponseList(HttpURLConnection conn) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
		 String output = "";
		    String line;
		    while ((line = br.readLine()) != null) {
		        output += line;
		    }

		    conn.disconnect();
		    
		    ObjectMapper objectMapper = new ObjectMapper();
		    objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		    TabelaReferencia[] tabelasReferencia = objectMapper.readValue(new String(output.getBytes()),  TabelaReferencia[].class);
		return  Arrays.asList(tabelasReferencia);
	}

}
