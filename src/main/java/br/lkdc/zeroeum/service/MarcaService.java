package br.lkdc.zeroeum.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.lkdc.zeroeum.model.Marca;
import br.lkdc.zeroeum.repository.IMarcaRepository;

@Service 
public class MarcaService {
	
	@Autowired
	private IMarcaRepository marcaRepository;
	
	@Transactional
	public void salvar(Marca marca) {
		this.marcaRepository.save(marca);
	}
	
	@Transactional
	public void salvar(List<Marca> marcas) {		
		this.marcaRepository.saveAll(marcas);
	}
	
	@Transactional
	public List<Marca> findAll() {
		List<Marca> marcas = new ArrayList<>(); 
		this.marcaRepository.findAll().forEach(marcas::add);
		return marcas;
	}
}
