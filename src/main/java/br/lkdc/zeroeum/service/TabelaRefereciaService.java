package br.lkdc.zeroeum.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.lkdc.zeroeum.model.TabelaReferencia;
import br.lkdc.zeroeum.repository.ITabelaReferenciaRepository;

@Service 
public class TabelaRefereciaService {
	
	@Autowired
	private ITabelaReferenciaRepository tabelaReferenciaRepository;
	
	@Transactional
	public void salvar(TabelaReferencia tabelaReferencia) {
		this.tabelaReferenciaRepository.save(tabelaReferencia);
	}
	
	@Transactional
	public void validarSalvarNovos(List<TabelaReferencia> tabelasReferencia) {
		Integer codigoTabelaAtual = this.tabelaReferenciaRepository.codigoAtualTabelaReferenciada();
		if(codigoTabelaAtual==null) {
			this.tabelaReferenciaRepository.saveAll(tabelasReferencia);			
		}else {
			List<TabelaReferencia> novosItens = tabelasReferencia
					.stream()
					.filter(c -> c.getCodigo() > codigoTabelaAtual)
					.collect(Collectors.toList());
			this.tabelaReferenciaRepository.saveAll(novosItens);			
			
		}
	}
	@Transactional
	public void salvar(List<TabelaReferencia> tabelasReferencia) {
			this.tabelaReferenciaRepository.saveAll(tabelasReferencia);			
	}
	
	@Transactional
	public List<TabelaReferencia> findAll() {
		List<TabelaReferencia> marcas = new ArrayList<>(); 
		this.tabelaReferenciaRepository.findAll().forEach(marcas::add);
		return marcas;
	}
}
