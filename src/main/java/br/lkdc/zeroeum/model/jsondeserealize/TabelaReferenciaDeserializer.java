package br.lkdc.zeroeum.model.jsondeserealize;

import java.io.IOException;

import javax.persistence.Column;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.IntNode;
import com.google.gson.annotations.Expose;

import br.lkdc.zeroeum.model.TabelaReferencia;

public class TabelaReferenciaDeserializer extends StdDeserializer<TabelaReferencia> {
	
	private static final long serialVersionUID = 9150770066798321804L;

	public TabelaReferenciaDeserializer() { 
        this(null); 
    } 

    public TabelaReferenciaDeserializer(Class<?> vc) { 
        super(vc); 
    }

    @Override
    public TabelaReferencia deserialize(JsonParser jp, DeserializationContext ctxt) 
      throws IOException, JsonProcessingException {
        JsonNode node = jp.getCodec().readTree(jp);
        Integer codigo = node.get("Codigo").asInt();
        String mesAno = node.get("Mes").asText();
        String[] items= mesAno.split("/");
        String mes = items[0];
        Integer ano = Integer.valueOf(items[1].subSequence(0, 4).toString());

        return new TabelaReferencia(codigo, mesAno,mes,ano);
    }
}
