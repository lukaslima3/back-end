package br.lkdc.zeroeum.model;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.gson.annotations.Expose;

import br.lkdc.zeroeum.model.jsondeserealize.TabelaReferenciaDeserializer;


@Entity
@JsonDeserialize(using = TabelaReferenciaDeserializer.class)
public class TabelaReferencia {
	
	@Id
	@Expose(serialize = true, deserialize = true) 
	@GeneratedValue(strategy=GenerationType.AUTO)
	private BigInteger id;
	
	@Column(name="codigo", nullable=false, length=100)
	@Expose(serialize = true, deserialize = true) 
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
	private Integer codigo;
	
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
	@Transient
	private String mesAno;
	
	@Column(name="ano", nullable=false)
	@Expose(serialize = true, deserialize = true)
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
	private Integer ano;
	
	@Column(name="mes", nullable=false)
	@Expose(serialize = true, deserialize = true)
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
	private String mes;

	public TabelaReferencia(Integer codigo, String mesAno,String mes,Integer ano) {
		this.mesAno=mesAno;
		this.codigo=codigo;
		this.mes=mes;
		this.ano=ano;
	}
	
	public TabelaReferencia() {
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getMesAno() {
		return mesAno;
	}

	public void setMesAno(String mesAno) {
		this.mesAno = mesAno;
	}


	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}
	
	
	
	
	
//	public Marca(String label, Integer value) {
//		super();
//		this.label = label;
//		this.value = value;
//	}
	
	
	
}
