package br.lkdc.zeroeum.model;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;


@Entity
public class Marca {
	
	@Id
	@Expose(serialize = false, deserialize = false) 
	@GeneratedValue(strategy=GenerationType.AUTO)
	private BigInteger id;
	
	@Column(name="descricao", nullable=false, length=100)
	@Expose(serialize = true, deserialize = true) 
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
	private String label;
	
	@Column(name="id_fipe", nullable=false)
	@Expose(serialize = true, deserialize = true)
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
	private Integer value;
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	
//	public Marca(String label, Integer value) {
//		super();
//		this.label = label;
//		this.value = value;
//	}
	
	
	
}
