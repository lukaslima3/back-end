package br.lkdc.zeroeum.model;

public class Veiculo {
	
	private String modelo;
	private Float preco;
	private String fabricante;
	
	
	
	public Veiculo(String modelo, Float preco, String fabricante) {
		super();
		this.modelo = modelo;
		this.preco = preco;
		this.fabricante = fabricante;
	}
	
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public Float getPreco() {
		return preco;
	}
	public void setPreco(Float preco) {
		this.preco = preco;
	}
	public String getFabricante() {
		return fabricante;
	}
	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}
	
	

}
