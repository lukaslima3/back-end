package br.lkdc.zeroeum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZeroeumApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZeroeumApplication.class, args);
	}

}
